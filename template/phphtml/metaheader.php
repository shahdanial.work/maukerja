<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php include('code/metatitle.php');?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="<?php include('code/metadescription.php');?>" />
	<link rel="canonical" href="<?php include('code/canonical.php');?>">
	
	<!-- Stylesheets -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flexslider.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/color/green.css">
	
	<meta property="og:title" content="<?php include('code/metatitle.php');?>" />
	<meta property="og:type" content="<?php if($id){echo "article";}else{echo"website";}?>" />
	<meta property="og:url" content="<?php include('code/canonical.php');?>" />
	<meta property="og:image" content="<?php include('code/fbimage.php');?>" />

	<!--[if IE 9]>
		<script src="js/media.match.min.js"></script>
	<![endif]-->
	<script async='ASYNC' src='//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'></script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=<?=$fbID;?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="main-wrapper">

	<header id="header" class="header-style-1">
