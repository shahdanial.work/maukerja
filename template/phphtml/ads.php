<?php
// tak perlu copy line <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
// ads atas sekali sebelum job list
// gunakan in-feed ads
$adsTop = '
<ins class="adsbygoogle"
style="display:block"
data-ad-format="fluid"
data-ad-layout-key="-6t+ed+2i-1n-4w"
data-ad-client="ca-pub-0277999766697937"
data-ad-slot="3519607064"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>';
// ads bawah sekali selepas job list
// gunakan in-feed ads

$adsBottom = '
<ins class="adsbygoogle"
style="display:block"
data-ad-format="fluid"
data-ad-layout-key="-6t+ed+2i-1n-4w"
data-ad-client="ca-pub-0277999766697937"
data-ad-slot="3519607064"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>';

// ads diantara job list
// gunakan in-feed ads

$adsCenter = '
<ins class="adsbygoogle"
style="display:block"
data-ad-format="fluid"
data-ad-layout-key="-6t+ed+2i-1n-4w"
data-ad-client="ca-pub-0277999766697937"
data-ad-slot="3519607064"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>';

// ads dalam single page sidebar
// guna text & display ads responsive 
$adsSidebar = '
<!-- JawatanMalaysia Responsive Display -->
<ins class="adsbygoogle"
style="display:block"
data-ad-client="ca-pub-0277999766697937"
data-ad-slot="8239766493"
data-ad-format="auto"
data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>';

// ads bawah tajuk kerja dalam single page - 2 ads
// ads 1 guna text & display responsive
// ads 2 guna text & display (link ads responsive)
$belowTitle = '
<!-- JawatanMalaysia Responsive Display -->
<ins class="adsbygoogle"
style="display:block"
data-ad-client="ca-pub-0277999766697937"
data-ad-slot="8239766493"
data-ad-format="auto"
data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<ins class="adsbygoogle"
style="display:block"
data-ad-client="ca-pub-0277999766697937"
data-ad-slot="9494822628"
data-ad-format="link"
data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>';

// ads di atas apply button
// ads guna text & display ( link ads 200x90)
$adsAboveApply = '
<!-- JawatanMalaysia Adlink Small -->
<ins class="adsbygoogle"
style="display:inline-block;width:200px;height:90px"
data-ad-client="ca-pub-0277999766697937"
data-ad-slot="5762201993"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>';
?>