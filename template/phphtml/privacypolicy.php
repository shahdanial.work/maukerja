<!-- end #header -->

	<div id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 page-content">
					<div class="title-lines">
						<h1 class="mt0">Privacy Policy for JawatanMalaysia.net</h1>
											
					</div>
					<div class="candidates-item candidates-single-item">
<p>If you require any more information or have any questions about our privacy policy, please feel free to contact us by Facebook Page. At JawatanMalaysia.net, the privacy of our visitors is of extreme importance to us. This privacy policy document outlines the types of personal information is received and collected by JawatanMalaysia.net and how it is used.

</p></div>
					
					<div class="candidates-item candidates-single-item">
						<h6 class="title"><a href="#">Log Files</a></h6>

						<p><br>Like many other Web sites, JawatanMalaysia.net makes use of log files. The information inside the log files includes internet protocol ( IP ) addresses, type of browser, Internet Service Provider ( ISP ), date/time stamp, referring/exit pages, and number of clicks to analyze trends, administer the site, track user's movement around the site, and gather demographic information. IP addresses, and other such information are not linked to any information that is personally identifiable.</p>

						<h6 class="title"><a href="#">DoubleClick DART Cookie</a></h6>
						<p><br>.:: Google, as a third party vendor, uses cookies to serve ads on JawatanMalaysia.net.
.:: Google's use of the DART cookie enables it to serve ads to users based on their visit to JawatanMalaysia.net and other sites on the Internet.
.:: Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy at the following URL - http://www.google.com/privacy_ads.html</p>

<p>Some of our advertising partners may use cookies and web beacons on our site. Our advertising partners include Innity, Indeed & Careerjet</p>

<p>These third-party ad servers or ad networks use technology to the advertisements and links that appear on JawatanMalaysia.net send directly to your browsers. They automatically receive your IP address when this occurs. Other technologies ( such as cookies, JavaScript, or Web Beacons ) may also be used by the third-party ad networks to measure the effectiveness of their advertisements and / or to personalize the advertising content that you see. </p>

<p>JawatanMalaysia.net has no access to or control over these cookies that are used by third-party advertisers. </p>

<p>You should consult the respective privacy policies of these third-party ad servers for more detailed information on their practices as well as for instructions about how to opt-out of certain practices. JawatanMalaysia.net's privacy policy does not apply to, and we cannot control the activities of, such other advertisers or web sites. </p>

<p>If you wish to disable cookies, you may do so through your individual browser options. More detailed information about cookie management with specific web browsers can be found at the browsers' respective websites.</p>

						
					</div>
					
				</div> <!-- end .page-content -->

<?php include('template/phphtml/sidebar.php'); ?>

			</div>
		</div> <!-- end .container -->

		
	</div> <!-- end #page-content -->

	