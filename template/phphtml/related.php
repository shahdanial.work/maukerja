<?php

//echo $what;
// Get the JSON

$json = file_get_contents('https://jawatanmalaysia.net/wp-json/wp/v2/posts/?categories=2&_embed=1&per_page=1&orderby=author');
$postss = json_decode($json);
$getRelated = $postss;

foreach ($getRelated as $p) {
    $titlerelated = $p->title->rendered;
	$linkrelated = $p->{'link'};
    $daterelated = date('M d', strtotime($p->date));
	$descrelated = $p->excerpt->rendered;
	$descrelated = substr($descrelated, 0, 200);
	$descrelated = substr($descrelated, 0, strrpos($descrelated, ' ')) . " ...";
    // Output the featured image (if there is one)
	$imagerelated = $p->_embedded->{'wp:featuredmedia'}{'0'}->media_details->sizes->thumbnail->source_url;
?>
<li>
									<div class="image">
										<img src="<?=$imagerelated;?>" alt="">
									</div>

									<div class="content">
										<h6><a href="<?=$linkrelated;?>"><?=$titlerelated;?></a></h6>
										<span class="location"><?=$daterelated;?></span>
										<p><?=$descrelated;?> <a href="<?=$linkrelated;?>" class="read-more">Read More</a></p>
									</div>
								</li>
<?php
}
?>
