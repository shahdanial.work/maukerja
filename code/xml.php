<?php

    /*--- CONTROLLER URL ---*/

    // 1. Page..
    $page = isset($_GET['page']) ? $_GET['page'] : '';

    // 2. Search..
    $search = isset($_GET['query']) ? $_GET['query'] : '';
    $search = str_replace('-',' ',$search);
    $canonicalsearch = strtolower(str_replace(" ","-",$search));
    $search = strtolower($search);
    //$search=ucwords($search);

    // 3. with location..
    $location = isset($_GET['loc']) ? $_GET['loc'] : '';
    $location = str_replace('-','',$location);
    $location = str_replace('','+',$location);
    $location = strtolower($location);
    //$location=ucwords($location);
    $canonicallocation = strtolower(str_replace(" ","-",$location));

    //4. kategori
    $kateGori = isset($_GET['cat']) ? $_GET['cat'] : '';
    $tYpe = isset($_GET['type']) ? $_GET['type'] : '';

    $id = isset($_GET['id']) ? $_GET['id'] : '';

    // print_r('<hr><br>');
    // print_r($id);
    // print_r('<br><hr>');

    if(!$page){
        $page = 1;
    }

    $startPage = $page-1;

    $urlMaukerja = "https://www.maukerja.my/api/job/listing?search_salary_currency=RM&sort_by=date&freshgraduate=false&fastresponse=false&hotjobs=false&urgentjobs=false&direct_contact=false&orderby=desc&start=$startPage&noOfRecords=10&pagename=joblisting&jobsearch_country_name=Malaysia&_token=&_method=GET";

    $start = 0;
    $end = 10;

    if($page > 1){
        $start = ($page-1)*10;
        $end = $start+10;
    }

    //negeri
    $negeri = array(
        "Johor",
        "Kelantan",
        "Kuala Lumpur",
        "Labuan",
        "Melaka",
        "Negeri Sembilan",
        "Pahang",
        "Perak",
        "Pulau Pinang",
        "Putrajaya",
        "Sabah",
        "Sarawak",
        "Selangor",
        "Terengganu",
        "Perlis",
        "Kedah"
    );

    $categoryName = array(
        '429' => 'Percetakan / Penerbitan',
        '428' => 'Polimer / Getah',
        '427' => 'Minyak / Gas / R & D',
        '433 '=>'Retail / Merchandise',
        '432 '=>'Pembaikan / Penyelenggaraan',
        '426 '=>'Mining',
        '421' => 'Undang-undang / Perundangan',
        '424' => 'Pengilangan / Pengeluaran',
        '433' => 'Sains & Teknologi',
        '435' => 'Penguatkuasaan / Penguatkuasaan Undang-Undang',
        '445' => 'Utiliti / Kuasa',
        '444' => 'Kayu / Serat / Kertas',
        '447 '=>'Industri lain',
        '449 '=>'Admin / Clerical',
        '448 '=>'Jualan / Biz',
        '443' => 'Pengangkutan / Logistik',
        '442' => 'Tembakau',
        '437' => 'Perkhidmatan Sosial / NGO',
        '436' => 'Semikonduktor',
        '439' => 'Pembrokeran / Sekuriti',
        '441' => 'Tekstil / Pakaian',
        '440' => 'Telekomunikasi',
        '418' => 'Insurans',
        '417' => 'Mgmt',
        '396' => 'Kereta / Automotif',
        '395' => 'Seni / Rekabentuk / Fesyen',
        '397' => 'Penerbangan / Penerbangan',
        '399' => 'Kecantikan / Kecergasan',
        '394' => 'Reka Bentuk Seni Bina',
        '393' => 'Pakaian',
        '385' => 'Pembelian',
        '390' => 'Perkhidmatan Perakaunan / Cukai',
        '392' => 'Pertanian / Unggas / Perikanan',
        '401' => 'Perniagaan / Mgmt Consulting',
        '402' => 'Pusat Panggilan / BPO',
        '412' => 'Perdagangan Umum & Borong', '413' => 'Government / Defense',
        '414' => 'Healthcare / Medical',
        '416' => 'Hotel / Perhotelan',
        '415' => 'Berat',
        '409' => 'Pembinaan / Bangunan',
        '403' => 'Kimia / Baja',
        '405 '=>'Produk Pengguna / FMCG',
        '406 '=>'Elec',
        '408' => 'Hiburan / Media',
        '407' => 'Kejuruteraan / Perunding Teknikal',
        '24 '=>'Khidmat Pelanggan / Meja Bantuan '
    );

    //$lokasi = in_array($location, $negeri);

    //type kerja
    $arrayType = array(
        '1' => 'Full Time',
        '2' => 'Part Time',
        '3' => 'Internship'
    );
    $typee = str_replace('','%20',@$arrayType[$tYpe]);

    //kategori
    $arrayKategori = array(
        'Call Center / BPO' => '1',
        'Advertising / Marketing' => '2',
        'Food & Beverage' => '3',
        'Beauty / Fitness' => '4',
        'Customer Service / Helpdesk' => '5',
        'Retail / Merchandise' => '6',
        'IT / Software' => '7',
        'Admin / Clerical' => '8',
        'Business / Mgmt Consulting' => '9',
        'Engineering / Technical Consulting' => '10',
        'IT / Hardware' => '11',
        'Accounting / Tax Services' => '12',
        'Manufacturing / Production' => '13',
        'Arts / Design / Fashion' => '14',
        'Sales / Biz Development' => '15',
        'Heavy Industrial / Machinery' => '16',
        'Exhibitions / Event Mgmt' => '17',
        'Education / Training' => '18',
        'HR Mgmt / Consulting' => '19',
        'Entertainment / Media' => '20',
        'Electrical & Electronics' => '21',
        'Purchase / Supply Chain' => '22',
        'Construction / Building' => '23',
        'Environment / Health / Safety' => '24',
        'Transportation / Logistics' => '25',
        'Banking / Finance' => '26',
        'Repair / Maintenance' => '27',
        'Social Services / NGO' => '28',
        'Automobile / Automotive' => '29',
        'Insurance' => '30','Other industries' => '31',
        'Agriculture / Poultry / Fisheries' => '32',
        'Architecture / Interior Design' => '33',
        'Travel / Tourism' => '34',
        'Hotel / Hospitality' => '35',
        'Property / Real Estate' => '36',
        'Gems / Jewellery' => '37',
        'Security / Law Enforcement' => '38',
        'Apparel' => '39',
        'Healthcare / Medical' => '40',
        'R&D' => '41',
        'Journalism' => '42',
        'General & Wholesale Trading' => '43',
        'Law / Legal' => '44',
        'Telecommunication' => '45',
        'Chemical / Fertilizers' => '46',
        'Sports' => '47',
        'BioTech / Pharmaceutical' => '48',
        'Consumer Products / FMCG' => '49',
        'Printing / Publishing' => '50',
        'Medical / Healthcare / Beauty' => '51',
        'Science & Technology' => '52'
    );

    if (in_array($kateGori, $arrayKategori)){
        $kateGoriSearch = array_search($kateGori, $arrayKategori);
    }

    if(!$search && !$location && !$kateGori && !$tYpe){
        // print_r('1111111111111');
        $res = curl_get($urlMaukerja);
        $decodejson = json_decode($res);
        $num = $decodejson->totalRecords;
    } elseif ($search && !$location && !$kateGori && !$tYpe) {
        // print_r('222222222222');
        $search = str_replace('','+',$search);
        $urlMaukerja .= "&keyword=$search";
        $res = curl_get($urlMaukerja);
        $decodejson = json_decode($res);
        $num = @$decodejson->totalRecords;
    } elseif (!$search && $location && !$kateGori && !$tYpe) {
        // print_r('33333333333');
        $urlMaukerja .= "&search_location=$location";
        $res = curl_get($urlMaukerja);
        $decodejson = json_decode($res);
        $num = $decodejson->totalRecords;
    } elseif ($search && $location && !$kateGori && !$tYpe) {
        // print_r('44444444444');
        $urlMaukerja .= "&keyword=$search&search_location=$location";
        $res = curl_get($urlMaukerja);
        $decodejson = json_decode($res);
        $num = $decodejson->totalRecords;
    } elseif($kateGori && !$search && !$location && !$tYpe){
        // print_r('5555555555555555');
        $urlMaukerja .= "&joblisting_category=$kateGori";
        $res = curl_get($urlMaukerja);
        $decodejson = json_decode($res);
        $num = $decodejson->totalRecords;
    } elseif($tYpe && !$kateGori && !$search && !$location){
        // print_r('66666666666666');
        $urlMaukerja .= "&job_type=$typee";
        $res = curl_get($urlMaukerja);
        $decodejson = json_decode($res);
        $num = $decodejson->totalRecords;
    }
    $bilMuka = ceil($num/10);
    $res = json_decode($res);
?>